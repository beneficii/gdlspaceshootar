﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class BuildButton : MonoBehaviour, IPointerClickHandler {

    public int BuildCost = 10;


    Button button;
    public TextMeshProUGUI CostText;
    public TextMeshProUGUI CaptionText;

    public PowerUpScript tower;


    private void Awake()
    {
        button = GetComponent<Button>();
        CostText.text = BuildCost.ToString() + "$";
        CaptionText.text = "Build";
    }

    private void OnEnable()
    {
        GameManager.MoneyUpdateAction += MoneyUpdated;
        if(GameManager.Current != null)
        {
            MoneyUpdated(GameManager.Current.Money);
        }
        else
        {
            MoneyUpdated(0);
        }
    }

    private void OnDisable()
    {
        GameManager.MoneyUpdateAction -= MoneyUpdated;
    }


    void MoneyUpdated(int money)
    {
        button.interactable = money >= BuildCost;
        CostText.color = money >= BuildCost ? Color.white : Color.red;
        CaptionText.enabled = money >= BuildCost;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!button.IsInteractable())
            return;

        if (!tower.gameObject.activeSelf)
        {
            tower.gameObject.SetActive(true);
            CaptionText.text = "Upgrade";
            GameManager.Current.Money-= BuildCost;
            Destroy(gameObject);
            //tower.Activate(GameManager.Current.player);
        } else
        {   
            //GameManager.Current.player.
        }
    }
}

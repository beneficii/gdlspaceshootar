﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effects/SummonEffect")]
public class SummonEffectData : TargetEffectData
{
    public GameObject SummonPrefab;

    public override void Affect(GameObject owner, GameObject target)
    {
        Instantiate(SummonPrefab, owner.transform.position, Quaternion.identity);
    }
}

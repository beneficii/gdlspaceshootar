﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PointEffectData : ScriptableObject {

    public abstract void Affect(Vector3 point);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Game/Ship")]
public class ShipData : ScriptableObject {


    public float size = 1f;
    public Color color;
    public float speed;
    public int reward = 1;
    public int health = 1;
    //public float fireRate;

    //public BulletData bulletData;

    public GameObject prefab;


    public virtual void Construct(Vector3 position, Transform parent)
    {
        GameObject instance = Instantiate(prefab, position, Quaternion.Euler(0, 180, 0), parent);
        instance.GetComponent<EnemyScript>().Init(speed);
        instance.GetComponent<EnemyScript>().reward = reward;
        instance.GetComponent<EnemyScript>().Health = health;
        instance.transform.localScale *= size;

        foreach (var rend in instance.GetComponentsInChildren<Renderer>())
        {
            rend.materials[0].SetColor("_SpecColor", color);
        }
        //instance.GetComponent<Renderer>().materials[0].SetColor("_SpecColor", color);

        /*foreach(var canon in instance.GetComponentsInChildren<CanonScript>())
        {
            canon.bulletData = bulletData;
            canon.rate = fireRate;
        }*/
    }

}

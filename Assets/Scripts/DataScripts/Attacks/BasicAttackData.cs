﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Attacks/Basic")]
public class BasicAttackData : AttackData {

    public GameObject bullet;
    public float speed;
    public Color color;


    public TargetEffectData collisionEffect;


    public override void Shoot(Camera cam)
    {
        GameObject instance = Instantiate(bullet, cam.transform.position + cam.transform.forward*3f , cam.transform.rotation);
        instance.GetComponent<Bullet>().Init(speed);
        instance.GetComponent<Bullet>().collisionEffect = collisionEffect;
        //instance.GetComponent<Renderer>().material.SetColor("_SpecColor", color);


    }
    
}

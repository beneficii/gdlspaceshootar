﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Attacks/GroundAttack")]
public class GroundAttackData : AttackData
{
    public int damage = 0;
    public GameObject SummonPrefab;
    public string targetLayer = "Ground";

    public override void Shoot(Camera cam)
    {
#if UNITY_EDITOR

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
#else
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
#endif
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 40f, LayerMask.GetMask(targetLayer), QueryTriggerInteraction.Collide))
        {
            if (damage > 0)
            {
                hit.transform.GetComponent<EnemyScript>().Health -= damage;
            }
            Instantiate(SummonPrefab, hit.point, SummonPrefab.transform.rotation);
            
        }
    }

}

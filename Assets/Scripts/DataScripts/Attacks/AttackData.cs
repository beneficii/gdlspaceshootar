﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackData : ScriptableObject {

    public AudioClip sound;
    public float rate;

    public abstract void Shoot(Camera cam);

}

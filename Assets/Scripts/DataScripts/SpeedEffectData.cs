﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effects/SpeedEffect")]
public class SpeedEffectData : TargetEffectData {

    public float multiplier = 0.5f;

    public override void Affect(GameObject owner, GameObject target)
    {
        if (target.CompareTag("Enemy"))
        {
            target.GetComponent<EnemyScript>().Speed *= 0.5f;
        }
    }
}

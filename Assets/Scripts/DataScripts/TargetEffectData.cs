﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effects/TargetEffect")]
public class TargetEffectData : ScriptableObject {

    public virtual void Affect(GameObject owner, GameObject target)
    {
        if (target.CompareTag("Enemy"))// || target.CompareTag("Ground"))
        {
            target.GetComponent<EnemyScript>().Health--;
        }
    }
}

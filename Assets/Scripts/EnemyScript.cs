﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public int reward;
    int health = 1;
    public int Health
    {
        get { return health; }
        set
        {

            health = value;
            if (health > 0)
            {
                //ToDo: Update health bar
            }
            else
            {
                GameManager.Current.Money += reward*3;
                GetComponent<Collider>().enabled = false;
                Instantiate(ExplosionPrefab, transform.position, Quaternion.identity, transform.parent);
                Destroy(gameObject, 0.1f);
            }
        }
    }

    float speed = 0;
    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = Mathf.Max(0.3f, value);
            GetComponent<Rigidbody>().velocity = transform.forward * value;
            GetComponent<Animator>().SetFloat("Speed", value);
        }
    }

    public GameObject ExplosionPrefab;

    public void Init(float speed, int hp = 1)
    {
        health = hp;
        Speed = speed;
        
    }

    void Update()
    {
        if (transform.position.z > GameManager.ZBound || transform.position.z < -GameManager.ZBound)
        {
            Destroy(gameObject);
        }
    }
}

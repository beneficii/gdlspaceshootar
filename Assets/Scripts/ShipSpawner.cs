﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSpawner : MonoBehaviour {

    public ShipData[] ships;

    float rate = 1f;
    float Rate
    {
        get
        {
            return rate;
        }
        set {
            rate = Mathf.Max(0.3f, value);
        }
    }
    float lastShot;

    private void Start()
    {
        InvokeRepeating("UpRate", 5, 10);
        lastShot = Time.time;
    }

    void UpRate()
    {
        rate *= 0.96f;
    }

    void Spawn()
    {
        if(ships.Length == 0)
        {
            return;
        }

        float x = Random.Range(-GameManager.XBound, GameManager.XBound);
        var shipData = ships[Random.Range(0, ships.Length)];

        shipData.Construct(new Vector3(x, transform.position.y, transform.position.z), transform);
    }

    private void FixedUpdate()
    {
        if (lastShot + rate <= Time.time)
        {
            Spawn();
            lastShot = Time.time;
        }
    }
}

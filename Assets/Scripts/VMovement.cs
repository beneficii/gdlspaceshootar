﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VMovement : MonoBehaviour {

    public void Init(float speed)
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    void Update()
    {
        if(transform.position.z > GameManager.ZBound || transform.position.z < -GameManager.ZBound)
        {
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputCtrl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    
    public ShooterScript shooter;

	void Awake () {
		shooter = FindObjectOfType(typeof(ShooterScript)) as ShooterScript;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        shooter.OnTouch(true);

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        shooter.OnTouch(false);
    }

}

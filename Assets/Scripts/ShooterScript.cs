﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShooterScript : MonoBehaviour {

    public AttackData attackData;
    public Image RechargeBar;


    Camera cam;
    AudioSource audioSrc;

    [HideInInspector] public bool shooting = false;


    LayerMask targetLayers;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        audioSrc = GetComponent<AudioSource>();
        targetLayers = LayerMask.GetMask("PowerUp");
    }

    public void Shoot()
    {
        if (attackData.sound)
        {
            audioSrc.PlayOneShot(attackData.sound);
        }
        attackData.Shoot(cam);
    }

    float lastShot;

    void Start()
    {
        lastShot = Time.time;
    }

    private void Update()
    {
        if(attackData == null)
        {
            return;
        }
        if (shooting)
        {
            if (lastShot + attackData.rate <= Time.time)
            {
                Shoot();
                lastShot = Time.time;
            }
            
        }
        RechargeBar.fillAmount = (Time.time - lastShot) / attackData.rate;
    }

    public void OnTouch( bool down)
    {
        if (down)
        {
#if UNITY_EDITOR

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
#else
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
#endif
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 40f, targetLayers, QueryTriggerInteraction.Collide))
            {
                hit.transform.GetComponent<PowerUpScript>().Activate(this);
                //Destroy(hit.transform.gameObject);
            }
            else
            {
                shooting = true;
            }
        }
        else
        {
            shooting = false;
        }
    }
}

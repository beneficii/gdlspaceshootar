﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameManager : MonoBehaviour {

    public const float ZBound = 12;
    public const float XBound = 9;
    public const float YBound = 2;

    public ShooterScript player;

    public static GameManager Current;

    public TMPro.TextMeshProUGUI MoneyText;

    public static bool Tracking = false;

    public int money = 9;
    public int Money
    {
        get
        {
            return money;
        }

        set
        {
            money = value;
            if(MoneyUpdateAction != null)
            {
                MoneyUpdateAction(value);
            }
            MoneyText.text = value.ToString() + "$";
        }

    }

    public delegate void UpdateAction(int x);
    public static UpdateAction MoneyUpdateAction;

    private void Awake()
    {
        QualitySettings.shadows = ShadowQuality.Disable;
        player = FindObjectOfType(typeof(ShooterScript)) as ShooterScript;
        Current = this;
        Money = Money;
        StartCoroutine(TutorialScenario());
    }

    public TMPro.TextMeshProUGUI TutorialText;
    public GameObject firstTurret;
    public GameObject ShipSpawner;
    public GameObject TutorialArrow;

    IEnumerator TutorialScenario()
    {
        TutorialText.text = "Welcome!\n Please find a marker";
        yield return new WaitUntil(()=>Tracking == true);
        TutorialText.text = "Let's start by building a tower!";
        yield return new WaitUntil(()=>firstTurret.activeSelf);
        TutorialText.text = "Good!\n Now target newly built tower and tap!";
        TutorialArrow.SetActive(true);
        TutorialArrow.transform.position = firstTurret.transform.position + Vector3.up*2;
        yield return new WaitUntil(() => player.attackData != null);
        TutorialArrow.SetActive(false);

        ShipSpawner.SetActive(true);
        TutorialText.text = "Tap to shoot!";
        yield return new WaitForSeconds(5f);
        TutorialText.text = "";

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public TargetEffectData collisionEffect;
    public GameObject muzzleflash;

    public void Init(float speed)
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    void Update()
    {
        if(transform.position.y < -GameManager.YBound)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        if (muzzleflash)
        {
            Instantiate(muzzleflash, transform.position, muzzleflash.transform.rotation);
        }
        if (collisionEffect != null)
        {
            collisionEffect.Affect(gameObject, other.gameObject);
        }
    }
}

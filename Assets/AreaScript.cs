﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaScript : MonoBehaviour {

    public float lifetime = 2f;
    public float radius = 2f;

    public TargetEffectData effectData;

    void Start () {
        
        foreach(var enemy in Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Enemy")))
        {
            effectData.Affect(gameObject, enemy.transform.gameObject);
        }

        Destroy(gameObject, lifetime);
	}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0.5f, 0.5f, 0, 0.2f);
        Gizmos.DrawSphere(transform.position, radius);
    }
}

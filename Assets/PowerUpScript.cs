﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PowerUpScript : MonoBehaviour {

    public AttackData reward;

    private void Start()
    {
        //GetComponent<Renderer>().material.SetColor("_SpecColor", ((BasicAttackData)reward).color); //if we fail we fail
    }

    public void Activate(ShooterScript shooter)
    {
        transform.DOShakeScale(0.2f, 0.2f, 2);
        shooter.attackData = reward;
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            //Destroy(gameObject);
            GameManager.Current.player.attackData = reward;
        }
    }*/
}
